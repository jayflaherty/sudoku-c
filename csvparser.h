#ifndef CSVPARSER_H
#define CSVPARSER_H
#endif

#include <stdio.h>

typedef struct csv_row {
	char **fields;
	size_t num_fields;
} csv_row;

typedef struct csv_parser {
	char *file_path;
	char delimiter;
	bool has_header;
	char *error_message;
	csv_row *header;
	FILE *file_handler;
	bool from_string;
	char *csv_string;
	int csv_string_iter;
} csv_parser;

// Public
csv_parser *init_parser(const char *file_path, const char *delimiter,bool is_header);

csv_parser *init_parser_from_string(const char *csv_string, const char *delimiter);

void free_parser(csv_parser *parser);

void free_row(csv_row * row);

csv_row *get_header(csv_parser *parser);

csv_row *get_row(csv_parser *parser);

size_t num_fields(csv_row *row);

char **get_fields(csv_row *row);

const char *get_error(csv_parser *parser);
