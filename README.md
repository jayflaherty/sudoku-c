# sudoku-c

Copyright (C) 2018  Jay Flaherty

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Using Sudoku as an example project to learn and practice C, make, git, etc..

The first attempt at solving a 4x4 and a 9x9 sudoku puzzle uses the brute force recursive backtracking approach using a simple one dimentional array.

This project uses the following external tools/libs:

* [csv_parser](https://github.com/JamesRamm/csv_parser)
* [CUnit](http://cunit.sourceforge.net/doc/index.html)

## Future Work on my road to mastering C

* ### Basic Strategies
    * Recursive backtracking via a two dimentional array
    * Naked Pairs
    * Naked Triples
    * Naked Quads
    * Hidden Pairs
    * Hidden Triples
    * Hidden Quads
    * Pointing Pairs
    * Box/Line Intersection

* ### Advanced Algorithms
    * [Constraint Propagation and Search](http://norvig.com/sudoku.html)
    * [Exact Cover Problem and Algorithm X solution using Dancing Links](https://arxiv.org/abs/cs/0011047)

* ### Solve Larger Sudoku Puzzles (i.e. 16x16)

* ### Sudoku Puzzle Generator
* ### Interactive Sudoku Game with hints
    * Ncurses
    * Qt
    * GTK+

* ### Make solution generic enough to solve similar problems
    * Pentomino Tiling
    * 8 Queens Puzzles

* ### Make code multi-platform
    * Autotools
    * CMake
    * Meson Build
