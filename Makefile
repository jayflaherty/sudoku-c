CC = gcc # gcc (GCC) 8.2.1 20180831
CFLAGS = -std=c11 -O2 -g -Wall -Wextra
INCLUDES = -I . # Directory for header file
LDLIBS = -lm
TEST_LDLIBS = -lcunit
OBJS = sudoku-solver.o sudoku.o csvparser.o # List of objects to be built
TEST_OBJS = sudoku_test.o sudoku.o csvparser.o # List of test objects to be built
.PHONY: all clean test # To declare all, clean, and test are not files

all: sudoku-solver

sudoku-solver: ${OBJS}
	@echo "Building sudoku-solver.."
	${CC} ${CFLAGS} ${INCLUDES} ${OBJS} -o sudoku-solver ${LDLIBS}

test: ${TEST_OBJS}
	@echo "Building sudoku-tester.."
	${CC} ${CFLAGS} ${INCLUDES} ${TEST_OBJS} -o sudoku-tester ${LDLIBS} ${TEST_LDLIBS}
	@echo "Running sudoku-tester.."
	-./sudoku-tester
	-rm -f sudoku-tester

# %.o: %.c  # % pattern wildcard matching
#	${CC} ${OPTIONS} -c $*.c ${INCLUDES}

clean:
	@echo "Cleaning up.."
	-rm -f *.o
	-rm -f sudoku-solver
