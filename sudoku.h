#ifndef SUDOKU_H
#define SUDOKU_H
#endif

extern size_t GRID_SIZE;
extern size_t GRID_WIDTH;

size_t return_row(size_t cell);

size_t return_col(size_t cell);

size_t return_block(size_t cell);

bool is_possible_row(size_t number, size_t row, const size_t grid[]);

bool is_possible_col(size_t number, size_t col, const size_t grid[]);

bool is_possible_block(size_t number, size_t block, const size_t grid[]);

bool is_possible_number(size_t number, size_t cell, const size_t grid[]);

bool is_completed_row(size_t row, const size_t grid[]);

bool is_completed_col(size_t col, const size_t grid[]);

bool is_completed_block(size_t block, const size_t grid[]);

bool arrays_equal(const size_t ordered_array[], const size_t unordered_array[],
		  const size_t length);

bool number_exists(size_t number, const size_t unordered_array[], size_t length);

size_t perfect_square(size_t grid_size);

void set_grid(size_t grid_size);

bool is_solved(const size_t grid[]);

bool find_available(const size_t grid[], size_t *cell);

void possible_values_for_cell(size_t cell, const size_t grid[], size_t values[]);

bool solve(size_t grid[], size_t grid_size);

void print_grid(const size_t grid[], const size_t grid_size);
