#include <stdio.h>		// for printf
#include <stdbool.h>		// for bool
#include <stdlib.h>		// for strtol

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
#include "sudoku.h"
#include "csvparser.h"

static size_t sudoku_grid_16[16] =
    { 2, 1, 0, 0, 0, 3, 2, 0, 0, 0, 0, 4, 1, 0, 0, 0 };
static size_t solved_sudoku_grid_16[16] =
    { 2, 1, 4, 3, 4, 3, 2, 1, 3, 2, 1, 4, 1, 4, 3, 2 };

static size_t sudoku_grid_81[81] =
{
     0, 0, 5, 9, 1, 0, 3, 0, 8,
     0, 0, 9, 4, 0, 3, 0, 6, 0,
     0, 2, 7, 5, 0, 0, 1, 0, 0,
     0, 3, 0, 0, 0, 0, 2, 0, 1,
     0, 0, 0, 8, 2, 0, 0, 0, 7,
     0, 0, 6, 0, 0, 7, 0, 0, 4,
     0, 0, 0, 0, 8, 0, 0, 0, 0,
     6, 4, 0, 1, 5, 0, 7, 0, 0,
     8, 9, 0, 0, 0, 0, 4, 2, 0
};

static size_t solved_sudoku_grid_81[81] =
{
     4, 6, 5, 9, 1, 2, 3, 7, 8,
     1, 8, 9, 4, 7, 3, 5, 6, 2,
     3, 2, 7, 5, 6, 8, 1, 4, 9,
     7, 3, 8, 6, 4, 5, 2, 9, 1,
     9, 5, 4, 8, 2, 1, 6, 3, 7,
     2, 1, 6, 3, 9, 7, 8, 5, 4,
     5, 7, 3, 2, 8, 4, 9, 1, 6,
     6, 4, 2, 1, 5, 9, 7, 8, 3,
     8, 9, 1, 7, 3, 6, 4, 2, 5
};

/* Test Suite setup and cleanup functions: */
int init_suite(void)
{
	return 0;
}

int clean_suite(void)
{
	return 0;
}

/************* Test case functions ****************/
void test_return_row_16(void)
{
	set_grid(16);	// required for testing methods outside of solve()
	CU_ASSERT_EQUAL(return_row(0), 0);
	CU_ASSERT_EQUAL(return_row(1), 0);
	CU_ASSERT_EQUAL(return_row(2), 0);
	CU_ASSERT_EQUAL(return_row(3), 0);

	CU_ASSERT_EQUAL(return_row(4), 1);
	CU_ASSERT_EQUAL(return_row(5), 1);
	CU_ASSERT_EQUAL(return_row(6), 1);
	CU_ASSERT_EQUAL(return_row(7), 1);

	CU_ASSERT_EQUAL(return_row(8), 2);
	CU_ASSERT_EQUAL(return_row(9), 2);
	CU_ASSERT_EQUAL(return_row(10), 2);
	CU_ASSERT_EQUAL(return_row(11), 2);

	CU_ASSERT_EQUAL(return_row(12), 3);
	CU_ASSERT_EQUAL(return_row(13), 3);
	CU_ASSERT_EQUAL(return_row(14), 3);
	CU_ASSERT_EQUAL(return_row(15), 3);
}

void test_return_row_81(void)
{
	set_grid(81);	// required for testing methods outside of solve()
	CU_ASSERT_EQUAL(return_row(0), 0);
	CU_ASSERT_EQUAL(return_row(1), 0);
	CU_ASSERT_EQUAL(return_row(2), 0);
	CU_ASSERT_EQUAL(return_row(3), 0);
	CU_ASSERT_EQUAL(return_row(4), 0);
	CU_ASSERT_EQUAL(return_row(5), 0);
	CU_ASSERT_EQUAL(return_row(6), 0);
	CU_ASSERT_EQUAL(return_row(7), 0);
	CU_ASSERT_EQUAL(return_row(8), 0);

	CU_ASSERT_EQUAL(return_row(9), 1);
	CU_ASSERT_EQUAL(return_row(10), 1);
	CU_ASSERT_EQUAL(return_row(11), 1);
	CU_ASSERT_EQUAL(return_row(12), 1);
	CU_ASSERT_EQUAL(return_row(13), 1);
	CU_ASSERT_EQUAL(return_row(14), 1);
	CU_ASSERT_EQUAL(return_row(15), 1);
	CU_ASSERT_EQUAL(return_row(16), 1);
	CU_ASSERT_EQUAL(return_row(17), 1);

	CU_ASSERT_EQUAL(return_row(18), 2);
	CU_ASSERT_EQUAL(return_row(19), 2);
	CU_ASSERT_EQUAL(return_row(20), 2);
	CU_ASSERT_EQUAL(return_row(21), 2);
	CU_ASSERT_EQUAL(return_row(22), 2);
	CU_ASSERT_EQUAL(return_row(23), 2);
	CU_ASSERT_EQUAL(return_row(24), 2);
	CU_ASSERT_EQUAL(return_row(25), 2);
	CU_ASSERT_EQUAL(return_row(26), 2);

	CU_ASSERT_EQUAL(return_row(27), 3);
	CU_ASSERT_EQUAL(return_row(28), 3);
	CU_ASSERT_EQUAL(return_row(29), 3);
	CU_ASSERT_EQUAL(return_row(30), 3);
	CU_ASSERT_EQUAL(return_row(31), 3);
	CU_ASSERT_EQUAL(return_row(32), 3);
	CU_ASSERT_EQUAL(return_row(33), 3);
	CU_ASSERT_EQUAL(return_row(34), 3);
	CU_ASSERT_EQUAL(return_row(35), 3);

	CU_ASSERT_EQUAL(return_row(36), 4);
	CU_ASSERT_EQUAL(return_row(37), 4);
	CU_ASSERT_EQUAL(return_row(38), 4);
	CU_ASSERT_EQUAL(return_row(39), 4);
	CU_ASSERT_EQUAL(return_row(40), 4);
	CU_ASSERT_EQUAL(return_row(41), 4);
	CU_ASSERT_EQUAL(return_row(42), 4);
	CU_ASSERT_EQUAL(return_row(43), 4);
	CU_ASSERT_EQUAL(return_row(44), 4);

	CU_ASSERT_EQUAL(return_row(45), 5);
	CU_ASSERT_EQUAL(return_row(46), 5);
	CU_ASSERT_EQUAL(return_row(47), 5);
	CU_ASSERT_EQUAL(return_row(48), 5);
	CU_ASSERT_EQUAL(return_row(49), 5);
	CU_ASSERT_EQUAL(return_row(50), 5);
	CU_ASSERT_EQUAL(return_row(51), 5);
	CU_ASSERT_EQUAL(return_row(52), 5);
	CU_ASSERT_EQUAL(return_row(53), 5);

	CU_ASSERT_EQUAL(return_row(54), 6);
	CU_ASSERT_EQUAL(return_row(55), 6);
	CU_ASSERT_EQUAL(return_row(56), 6);
	CU_ASSERT_EQUAL(return_row(57), 6);
	CU_ASSERT_EQUAL(return_row(58), 6);
	CU_ASSERT_EQUAL(return_row(59), 6);
	CU_ASSERT_EQUAL(return_row(60), 6);
	CU_ASSERT_EQUAL(return_row(61), 6);
	CU_ASSERT_EQUAL(return_row(62), 6);

	CU_ASSERT_EQUAL(return_row(63), 7);
	CU_ASSERT_EQUAL(return_row(64), 7);
	CU_ASSERT_EQUAL(return_row(65), 7);
	CU_ASSERT_EQUAL(return_row(66), 7);
	CU_ASSERT_EQUAL(return_row(67), 7);
	CU_ASSERT_EQUAL(return_row(68), 7);
	CU_ASSERT_EQUAL(return_row(69), 7);
	CU_ASSERT_EQUAL(return_row(70), 7);
	CU_ASSERT_EQUAL(return_row(71), 7);

	CU_ASSERT_EQUAL(return_row(72), 8);
	CU_ASSERT_EQUAL(return_row(73), 8);
	CU_ASSERT_EQUAL(return_row(74), 8);
	CU_ASSERT_EQUAL(return_row(75), 8);
	CU_ASSERT_EQUAL(return_row(76), 8);
	CU_ASSERT_EQUAL(return_row(77), 8);
	CU_ASSERT_EQUAL(return_row(78), 8);
	CU_ASSERT_EQUAL(return_row(79), 8);
	CU_ASSERT_EQUAL(return_row(80), 8);
}

void test_return_col_16(void)
{
	set_grid(16);	// required for testing methods outside of solve()
	CU_ASSERT_EQUAL(return_col(0), 0);
	CU_ASSERT_EQUAL(return_col(1), 1);
	CU_ASSERT_EQUAL(return_col(2), 2);
	CU_ASSERT_EQUAL(return_col(3), 3);

	CU_ASSERT_EQUAL(return_col(4), 0);
	CU_ASSERT_EQUAL(return_col(5), 1);
	CU_ASSERT_EQUAL(return_col(6), 2);
	CU_ASSERT_EQUAL(return_col(7), 3);

	CU_ASSERT_EQUAL(return_col(8), 0);
	CU_ASSERT_EQUAL(return_col(9), 1);
	CU_ASSERT_EQUAL(return_col(10), 2);
	CU_ASSERT_EQUAL(return_col(11), 3);

	CU_ASSERT_EQUAL(return_col(12), 0);
	CU_ASSERT_EQUAL(return_col(13), 1);
	CU_ASSERT_EQUAL(return_col(14), 2);
	CU_ASSERT_EQUAL(return_col(15), 3);
}

void test_return_col_81(void)
{
	set_grid(81);	// required for testing methods outside of solve()
	CU_ASSERT_EQUAL(return_col(0), 0);
	CU_ASSERT_EQUAL(return_col(1), 1);
	CU_ASSERT_EQUAL(return_col(2), 2);
	CU_ASSERT_EQUAL(return_col(3), 3);
	CU_ASSERT_EQUAL(return_col(4), 4);
	CU_ASSERT_EQUAL(return_col(5), 5);
	CU_ASSERT_EQUAL(return_col(6), 6);
	CU_ASSERT_EQUAL(return_col(7), 7);
	CU_ASSERT_EQUAL(return_col(8), 8);

	CU_ASSERT_EQUAL(return_col(9), 0);
	CU_ASSERT_EQUAL(return_col(10), 1);
	CU_ASSERT_EQUAL(return_col(11), 2);
	CU_ASSERT_EQUAL(return_col(12), 3);
	CU_ASSERT_EQUAL(return_col(13), 4);
	CU_ASSERT_EQUAL(return_col(14), 5);
	CU_ASSERT_EQUAL(return_col(15), 6);
	CU_ASSERT_EQUAL(return_col(16), 7);
	CU_ASSERT_EQUAL(return_col(17), 8);

	CU_ASSERT_EQUAL(return_col(18), 0);
	CU_ASSERT_EQUAL(return_col(19), 1);
	CU_ASSERT_EQUAL(return_col(20), 2);
	CU_ASSERT_EQUAL(return_col(21), 3);
	CU_ASSERT_EQUAL(return_col(22), 4);
	CU_ASSERT_EQUAL(return_col(23), 5);
	CU_ASSERT_EQUAL(return_col(24), 6);
	CU_ASSERT_EQUAL(return_col(25), 7);
	CU_ASSERT_EQUAL(return_col(26), 8);

	CU_ASSERT_EQUAL(return_col(27), 0);
	CU_ASSERT_EQUAL(return_col(28), 1);
	CU_ASSERT_EQUAL(return_col(29), 2);
	CU_ASSERT_EQUAL(return_col(30), 3);
	CU_ASSERT_EQUAL(return_col(31), 4);
	CU_ASSERT_EQUAL(return_col(32), 5);
	CU_ASSERT_EQUAL(return_col(33), 6);
	CU_ASSERT_EQUAL(return_col(34), 7);
	CU_ASSERT_EQUAL(return_col(35), 8);

	CU_ASSERT_EQUAL(return_col(36), 0);
	CU_ASSERT_EQUAL(return_col(37), 1);
	CU_ASSERT_EQUAL(return_col(38), 2);
	CU_ASSERT_EQUAL(return_col(39), 3);
	CU_ASSERT_EQUAL(return_col(40), 4);
	CU_ASSERT_EQUAL(return_col(41), 5);
	CU_ASSERT_EQUAL(return_col(42), 6);
	CU_ASSERT_EQUAL(return_col(43), 7);
	CU_ASSERT_EQUAL(return_col(44), 8);

	CU_ASSERT_EQUAL(return_col(45), 0);
	CU_ASSERT_EQUAL(return_col(46), 1);
	CU_ASSERT_EQUAL(return_col(47), 2);
	CU_ASSERT_EQUAL(return_col(48), 3);
	CU_ASSERT_EQUAL(return_col(49), 4);
	CU_ASSERT_EQUAL(return_col(50), 5);
	CU_ASSERT_EQUAL(return_col(51), 6);
	CU_ASSERT_EQUAL(return_col(52), 7);
	CU_ASSERT_EQUAL(return_col(53), 8);

	CU_ASSERT_EQUAL(return_col(54), 0);
	CU_ASSERT_EQUAL(return_col(55), 1);
	CU_ASSERT_EQUAL(return_col(56), 2);
	CU_ASSERT_EQUAL(return_col(57), 3);
	CU_ASSERT_EQUAL(return_col(58), 4);
	CU_ASSERT_EQUAL(return_col(59), 5);
	CU_ASSERT_EQUAL(return_col(60), 6);
	CU_ASSERT_EQUAL(return_col(61), 7);
	CU_ASSERT_EQUAL(return_col(62), 8);

	CU_ASSERT_EQUAL(return_col(63), 0);
	CU_ASSERT_EQUAL(return_col(64), 1);
	CU_ASSERT_EQUAL(return_col(65), 2);
	CU_ASSERT_EQUAL(return_col(66), 3);
	CU_ASSERT_EQUAL(return_col(67), 4);
	CU_ASSERT_EQUAL(return_col(68), 5);
	CU_ASSERT_EQUAL(return_col(69), 6);
	CU_ASSERT_EQUAL(return_col(70), 7);
	CU_ASSERT_EQUAL(return_col(71), 8);

	CU_ASSERT_EQUAL(return_col(72), 0);
	CU_ASSERT_EQUAL(return_col(73), 1);
	CU_ASSERT_EQUAL(return_col(74), 2);
	CU_ASSERT_EQUAL(return_col(75), 3);
	CU_ASSERT_EQUAL(return_col(76), 4);
	CU_ASSERT_EQUAL(return_col(77), 5);
	CU_ASSERT_EQUAL(return_col(78), 6);
	CU_ASSERT_EQUAL(return_col(79), 7);
	CU_ASSERT_EQUAL(return_col(80), 8);
}

void test_return_block_16(void)
{
	CU_ASSERT_EQUAL(return_block(0), 0);
	CU_ASSERT_EQUAL(return_block(1), 0);
	CU_ASSERT_EQUAL(return_block(2), 1);
	CU_ASSERT_EQUAL(return_block(3), 1);
	CU_ASSERT_EQUAL(return_block(4), 0);
	CU_ASSERT_EQUAL(return_block(5), 0);
	CU_ASSERT_EQUAL(return_block(6), 1);
	CU_ASSERT_EQUAL(return_block(7), 1);
	CU_ASSERT_EQUAL(return_block(8), 2);
	CU_ASSERT_EQUAL(return_block(9), 2);
	CU_ASSERT_EQUAL(return_block(10), 3);
	CU_ASSERT_EQUAL(return_block(11), 3);
	CU_ASSERT_EQUAL(return_block(12), 2);
	CU_ASSERT_EQUAL(return_block(13), 2);
	CU_ASSERT_EQUAL(return_block(14), 3);
	CU_ASSERT_EQUAL(return_block(15), 3);
}

void test_return_block_81(void)
{
	set_grid(81);	// required for testing methods outside of solve()
	CU_ASSERT_EQUAL(return_block(0), 0);
	CU_ASSERT_EQUAL(return_block(1), 0);
	CU_ASSERT_EQUAL(return_block(2), 0);
	CU_ASSERT_EQUAL(return_block(3), 1);
	CU_ASSERT_EQUAL(return_block(4), 1);
	CU_ASSERT_EQUAL(return_block(5), 1);
	CU_ASSERT_EQUAL(return_block(6), 2);
	CU_ASSERT_EQUAL(return_block(7), 2);
	CU_ASSERT_EQUAL(return_block(8), 2);

	CU_ASSERT_EQUAL(return_block(9), 0);
	CU_ASSERT_EQUAL(return_block(10), 0);
	CU_ASSERT_EQUAL(return_block(11), 0);
	CU_ASSERT_EQUAL(return_block(12), 1);
	CU_ASSERT_EQUAL(return_block(13), 1);
	CU_ASSERT_EQUAL(return_block(14), 1);
	CU_ASSERT_EQUAL(return_block(15), 2);
	CU_ASSERT_EQUAL(return_block(16), 2);
	CU_ASSERT_EQUAL(return_block(17), 2);

	CU_ASSERT_EQUAL(return_block(18), 0);
	CU_ASSERT_EQUAL(return_block(19), 0);
	CU_ASSERT_EQUAL(return_block(20), 0);
	CU_ASSERT_EQUAL(return_block(21), 1);
	CU_ASSERT_EQUAL(return_block(22), 1);
	CU_ASSERT_EQUAL(return_block(23), 1);
	CU_ASSERT_EQUAL(return_block(24), 2);
	CU_ASSERT_EQUAL(return_block(25), 2);
	CU_ASSERT_EQUAL(return_block(26), 2);

	CU_ASSERT_EQUAL(return_block(27), 3);
	CU_ASSERT_EQUAL(return_block(28), 3);
	CU_ASSERT_EQUAL(return_block(29), 3);
	CU_ASSERT_EQUAL(return_block(30), 4);
	CU_ASSERT_EQUAL(return_block(31), 4);
	CU_ASSERT_EQUAL(return_block(32), 4);
	CU_ASSERT_EQUAL(return_block(33), 5);
	CU_ASSERT_EQUAL(return_block(34), 5);
	CU_ASSERT_EQUAL(return_block(35), 5);

	CU_ASSERT_EQUAL(return_block(36), 3);
	CU_ASSERT_EQUAL(return_block(37), 3);
	CU_ASSERT_EQUAL(return_block(38), 3);
	CU_ASSERT_EQUAL(return_block(39), 4);
	CU_ASSERT_EQUAL(return_block(40), 4);
	CU_ASSERT_EQUAL(return_block(41), 4);
	CU_ASSERT_EQUAL(return_block(42), 5);
	CU_ASSERT_EQUAL(return_block(43), 5);
	CU_ASSERT_EQUAL(return_block(44), 5);

	CU_ASSERT_EQUAL(return_block(45), 3);
	CU_ASSERT_EQUAL(return_block(46), 3);
	CU_ASSERT_EQUAL(return_block(47), 3);
	CU_ASSERT_EQUAL(return_block(48), 4);
	CU_ASSERT_EQUAL(return_block(49), 4);
	CU_ASSERT_EQUAL(return_block(50), 4);
	CU_ASSERT_EQUAL(return_block(51), 5);
	CU_ASSERT_EQUAL(return_block(52), 5);
	CU_ASSERT_EQUAL(return_block(53), 5);

	CU_ASSERT_EQUAL(return_block(54), 6);
	CU_ASSERT_EQUAL(return_block(55), 6);
	CU_ASSERT_EQUAL(return_block(56), 6);
	CU_ASSERT_EQUAL(return_block(57), 7);
	CU_ASSERT_EQUAL(return_block(58), 7);
	CU_ASSERT_EQUAL(return_block(59), 7);
	CU_ASSERT_EQUAL(return_block(60), 8);
	CU_ASSERT_EQUAL(return_block(61), 8);
	CU_ASSERT_EQUAL(return_block(62), 8);

	CU_ASSERT_EQUAL(return_block(63), 6);
	CU_ASSERT_EQUAL(return_block(64), 6);
	CU_ASSERT_EQUAL(return_block(65), 6);
	CU_ASSERT_EQUAL(return_block(66), 7);
	CU_ASSERT_EQUAL(return_block(67), 7);
	CU_ASSERT_EQUAL(return_block(68), 7);
	CU_ASSERT_EQUAL(return_block(69), 8);
	CU_ASSERT_EQUAL(return_block(70), 8);
	CU_ASSERT_EQUAL(return_block(71), 8);

	CU_ASSERT_EQUAL(return_block(72), 6);
	CU_ASSERT_EQUAL(return_block(73), 6);
	CU_ASSERT_EQUAL(return_block(74), 6);
	CU_ASSERT_EQUAL(return_block(75), 7);
	CU_ASSERT_EQUAL(return_block(76), 7);
	CU_ASSERT_EQUAL(return_block(77), 7);
	CU_ASSERT_EQUAL(return_block(78), 8);
	CU_ASSERT_EQUAL(return_block(79), 8);
	CU_ASSERT_EQUAL(return_block(80), 8);
}

void test_is_possible_number_16(void)
{
	set_grid(16);	// required for testing methods outside of solve()
	CU_ASSERT_TRUE(is_possible_number(3, 2, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(4, 2, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(3, 3, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(4, 4, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(1, 7, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(3, 8, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(2, 9, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(1, 10, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(3, 10, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(2, 13, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(4, 13, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(3, 14, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(2, 15, sudoku_grid_16));
	CU_ASSERT_TRUE(is_possible_number(3, 15, sudoku_grid_16));
}

void test_is_possible_number_81(void)
{
	set_grid(81);	// required for testing methods outside of solve()
	CU_ASSERT_TRUE(is_possible_number(4, 0, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(6, 1, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(2, 5, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(7, 7, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(1, 9, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(8, 10, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(7, 13, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(5, 15, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(2, 17, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(3, 18, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(6, 22, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(8, 23, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(4, 25, sudoku_grid_81));
	CU_ASSERT_TRUE(is_possible_number(9, 26, sudoku_grid_81));
}

void test_possible_values_for_cell_16(void)
{
	set_grid(16);	// required for testing methods outside of solve()
	const size_t test_values[4] = { 3, 4, 0, 0 };
	size_t possible_values[4] = { 0 };
	possible_values_for_cell(2, sudoku_grid_16, possible_values);
	CU_ASSERT_TRUE(memcmp(test_values, possible_values, sizeof(test_values))
		       == 0);
}

void test_possible_values_for_cell_81(void)
{
	set_grid(81);	// required for testing methods outside of solve()
	const size_t test_values[9] = { 4, 5, 7, 9, 0, 0, 0, 0, 0 };
	size_t possible_values[9] = { 0 };
	possible_values_for_cell(27, sudoku_grid_81, possible_values);
	CU_ASSERT_TRUE(memcmp(test_values, possible_values, sizeof(test_values))
		       == 0);
}


void test_is_solved_16(void)
{
	set_grid(16);
	CU_ASSERT_TRUE(is_solved(solved_sudoku_grid_16));
}

void test_is_solved_81(void)
{
	set_grid(81);
	CU_ASSERT_TRUE(is_solved(solved_sudoku_grid_81));
}

void test_solve_16(void)
{
	CU_ASSERT_TRUE(solve(sudoku_grid_16, 16));
	CU_ASSERT_TRUE(memcmp
		       (solved_sudoku_grid_16, sudoku_grid_16,
			sizeof(solved_sudoku_grid_16)) == 0);
	print_grid(sudoku_grid_16, 16);
}

void test_solve_81(void)
{
	CU_ASSERT_TRUE(solve(sudoku_grid_81, 81));
	CU_ASSERT_TRUE(memcmp
		       (solved_sudoku_grid_81, sudoku_grid_81,
			sizeof(solved_sudoku_grid_81)) == 0);
	print_grid(sudoku_grid_81, 81);
}

void test_solve_from_csv_file(void)
{
	csv_parser *csvparser = init_parser("sudoku-puzzles.csv", ",", 0);
	csv_row *row;
	while ((row = get_row(csvparser))) {
		size_t grid_size = num_fields(row);
		if (grid_size == 16 || grid_size == 81) {
			size_t test_sudoku[grid_size];
			for (size_t i = 0; i < num_fields(row); i++) {
				char **row_fields = get_fields(row);
				char *leftover_str;
				long int field =
				    strtol(row_fields[i], &leftover_str, 10);
				test_sudoku[i] = (int)field;
			}
			print_grid(test_sudoku, grid_size);
			CU_ASSERT_TRUE(solve(test_sudoku, grid_size));
		}
		free_row(row);
	}
	free_parser(csvparser);
}

int main(void)
{
	CU_pSuite pSuite = NULL;

	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return CU_get_error();
	}

	/* add a suite to the registry */
	pSuite = CU_add_suite("sudoku_test_suite", init_suite, clean_suite);
	if (NULL == pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* add the tests to the suite */
	if ((NULL == CU_add_test(pSuite, "test_return_row_16", test_return_row_16)) ||
	    (NULL == CU_add_test(pSuite, "test_return_col_16", test_return_col_16)) ||
	    (NULL == CU_add_test(pSuite, "test_return_block_16", test_return_block_16)) ||
	    (NULL == CU_add_test(pSuite, "test_return_row_81", test_return_row_81)) ||
    	    (NULL == CU_add_test(pSuite, "test_return_col_81", test_return_col_81)) ||
    	    (NULL == CU_add_test(pSuite, "test_return_block_81", test_return_block_81)) ||
	    (NULL == CU_add_test(pSuite, "test_is_possible_number_16", test_is_possible_number_16)) ||
	    (NULL == CU_add_test(pSuite, "test_is_possible_number_81", test_is_possible_number_81)) ||
	    (NULL == CU_add_test(pSuite, "test_possible_values_for_cell_16", test_possible_values_for_cell_16)) ||
	    (NULL == CU_add_test(pSuite, "test_possible_values_for_cell_81", test_possible_values_for_cell_81)) ||
	    (NULL == CU_add_test(pSuite, "test_is_solved_16", test_is_solved_16)) ||
	    (NULL == CU_add_test(pSuite, "test_is_solved_81", test_is_solved_81)) ||
	    (NULL == CU_add_test(pSuite, "test_solve_16", test_solve_16)) ||
	    (NULL == CU_add_test(pSuite, "test_solve_81", test_solve_81)) ||
	    (NULL == CU_add_test(pSuite, "test_solve_from_csv_file", test_solve_from_csv_file))) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	// Run all tests using the basic interface
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	printf("\n");

	CU_basic_show_failures(CU_get_failure_list());
	printf("\n\n");

	/* Clean up registry and return */
	CU_cleanup_registry();
	return CU_get_error();
}
