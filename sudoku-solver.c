#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include "sudoku.h"
#include "csvparser.h"

int main(int argc, char *argv[])
{
	int opt = 0;
	char *file_name = NULL;
	while ((opt = getopt(argc, argv, "f:")) != -1) {
		switch (opt) {
		case 'f':
			file_name = optarg;
			break;
		default:
			printf("Usage sudoku-solver -f path/to/puzzlefile\n");
			return EXIT_FAILURE;
		}
	}

	csv_parser *csvparser = init_parser(file_name, ",", 0);
	csv_row *row;
	while ((row = get_row(csvparser))) {
		size_t grid_size = num_fields(row);
		if (grid_size == 16 || grid_size == 81) {
			size_t sudoku[grid_size];
			for (size_t i = 0; i < num_fields(row); i++) {
				char **row_fields = get_fields(row);
				char *leftover_str;
				long int field = strtol(row_fields[i], &leftover_str, 10);
				sudoku[i] = (int)field;
			}
			print_grid(sudoku, grid_size);
			if (solve(sudoku, grid_size)) {
				print_grid(sudoku, grid_size);
			} else {
				printf("Unable to solve sudoku puzzle!\n");
			}
		} else {
			printf("Invalid puzzle size. Number of cells for sudoku puzzles are 16 (4x4) or 81 (9x9)\n");
		}
		free_row(row);
	}
	free_parser(csvparser);

	return EXIT_SUCCESS;
}
