#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include "sudoku.h"

size_t GRID_SIZE;
size_t GRID_WIDTH;
size_t GRID_WIDTH_SQUARE;

/**
 * given a index into the grid array,
 * return the row number of the sudoku grid
 */
size_t return_row(size_t cell)
{
	return cell / GRID_WIDTH;
}

/**
 * given a index into the grid array,
 * return the column number of the sudoku grid
 */
size_t return_col(size_t cell)
{
	return cell % GRID_WIDTH;
}

/**
 * given a index into the grid array,
 * return the block number of the sudoku grid
 */
size_t return_block(size_t cell)
{
	return ((return_row(cell) / GRID_WIDTH_SQUARE * GRID_WIDTH_SQUARE
		 + return_col(cell) / GRID_WIDTH_SQUARE));
}

/**
 * given a cell number and a row number,
 * check if its a valid number for that row
 */
bool is_possible_row(size_t number, size_t row, const size_t grid[])
{
	for (size_t i = 0; i < GRID_WIDTH; i++) {
		if (grid[(row * GRID_WIDTH) + i] == number) {
			return false;
		}
	}
	return true;
}

/**
 * given a cell number and a column number,
 * check if its a valid number for that column
 */
bool is_possible_col(size_t number, size_t col, const size_t grid[])
{
	for (size_t i = 0; i < GRID_WIDTH; i++) {
		if (grid[col + (GRID_WIDTH * i)] == number) {
			return false;
		}
	}
	return true;
}

/**
 * given a cell number and a block number,
 * check if its a valid number for that block
 */
bool is_possible_block(size_t number, size_t block, const size_t grid[])
{
	for (size_t i = 0; i < GRID_SIZE; i++) {
		if (return_block(i) == block && grid[i] == number) {
			return false;
		}
	}
	return true;
}

/**
 * given a cell number, check if its a valid number
 * for the row, column, block
 */
bool is_possible_number(size_t number, size_t cell, const size_t grid[])
{
	return (is_possible_row(number, return_row(cell), grid) &&
		is_possible_col(number, return_col(cell), grid) &&
		is_possible_block(number, return_block(cell), grid)
	    );
}

/**
 * given a row number, check if all cells have been solved for that row
 */
bool is_completed_row(size_t row, const size_t solved_grid[])
{
	size_t ordered_row[GRID_WIDTH];
	size_t completed_row[GRID_WIDTH];

	for (size_t i = 0; i < GRID_WIDTH; i++) {
		ordered_row[i] = i + 1;
		completed_row[i] = solved_grid[(row * GRID_WIDTH) + i];
	}

	return arrays_equal(ordered_row, completed_row, GRID_WIDTH);
}

/**
 * given a col number, check if all cells have been solved for that column
 */
bool is_completed_col(size_t col, const size_t solved_grid[])
{
	size_t ordered_col[GRID_WIDTH];
	size_t completed_col[GRID_WIDTH];

	for (size_t i = 0; i < GRID_WIDTH; i++) {
		ordered_col[i] = i + 1;
		completed_col[i] = solved_grid[col + (i * GRID_WIDTH)];
	}
	return arrays_equal(ordered_col, completed_col, GRID_WIDTH);
}

/**
 * given a block number, check if all cells have been solved for that block
 */
bool is_completed_block(size_t block, const size_t solved_grid[])
{
	size_t ordered_block[GRID_WIDTH];
	size_t completed_block[GRID_WIDTH];

	size_t index = 0;
	for (size_t i = 0; i < GRID_SIZE; i++) {
		size_t returned_block = return_block(i);
		if (returned_block == block) {
			ordered_block[index] = index + 1;
			completed_block[index] = solved_grid[i];
			index++;
		}
	}
	return arrays_equal(ordered_block, completed_block, GRID_WIDTH);
}

/**
 * Given a sudoku grid, check if it has been solved.
 */
bool is_solved(const size_t solved_grid[])
{
	for (size_t i = 0; i < GRID_WIDTH; i++) {
		if (is_completed_row(i, solved_grid) == false ||
		    is_completed_col(i, solved_grid) == false ||
		    is_completed_block(i, solved_grid) == false) {
			return false;
		}
	}
	return true;
}

/**
 * function to check if all cells are assigned or not
 * if there is any unassigned cell
 * then this function will change the values of the cell accordingly
 */
bool find_available(const size_t solved_grid[], size_t *cell)
{
	for (size_t i = 0; i < GRID_SIZE; i++) {
		if (solved_grid[i] == 0) {
			*cell = i;
			return true;
		}
	}
	return false;
}

/**
 * utility method to compare the contents of 2 arrays.
 * i.e. they have the same members irregardless of order
 */
bool arrays_equal(const size_t ordered_array[], const size_t unordered_array[], size_t length)
{
	for (size_t i = 0; i < length; i++) {
		if (number_exists(ordered_array[i], unordered_array, GRID_WIDTH)
		    == false) {
			return false;
		}
	}
	return true;
}

/**
 * Given a number and an unordered array, check if number exists in the array.
 */
bool number_exists(size_t number, const size_t unordered_array[], size_t length)
{
	for (size_t i = 0; i < length; i++) {
		if (unordered_array[i] == number) {
			return true;
		}
	}
	return false;
}

void set_grid(size_t grid_size)
{
	GRID_SIZE = grid_size;
	GRID_WIDTH = sqrt(GRID_SIZE);
	GRID_WIDTH_SQUARE = sqrt(GRID_WIDTH);
}

/**
 * Given a grid cell, return array of all remaining possible
 * values for that cell
 */
void possible_values_for_cell(size_t cell, const size_t grid[], size_t possible_values[])
{
	size_t index = 0;
	for (size_t i = 1; i <= GRID_WIDTH; i++) {
		if (is_possible_number(i, cell, grid)) {
			possible_values[index++] = i;
		}
	}
}

/**
 * Given a paritally solved grid, recursively solve the sudoku grid using
 * the brute force backtracking method.
 */
bool solve(size_t grid[], size_t grid_size)
{
	set_grid(grid_size);
	size_t cell = 0;

	if (find_available(grid, &cell) == false && is_solved(grid)) {
		print_grid(grid, GRID_SIZE);
		return true;
	}

	for (size_t number = 1; number <= GRID_WIDTH; number++) {
		if (is_possible_number(number, cell, grid)) {
			grid[cell] = number;
			if (solve(grid, GRID_SIZE)) {
				return true;
			}
			grid[cell] = 0;
		}
	}
	return false;
}

void print_grid(const size_t grid[], size_t grid_size)
{
	set_grid(grid_size);
	printf("\n");
	for (size_t i = 0; i < GRID_WIDTH; i++) {
		for (size_t j = 0; j < GRID_WIDTH; j++) {
			printf("%2zu", grid[(i * GRID_WIDTH) + j]);
		}
		printf("\n");
	}
	printf("\n");
}
