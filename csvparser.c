#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "csvparser.h"

// Private
static bool delimiter_is_accepted(const char *delimiter);

static void set_error(csv_parser *parser, const char *error_message);

csv_row *_get_row(csv_parser *parser);

csv_parser *init_parser(const char *file_path, const char *delimiter, bool has_header)
{
	csv_parser *parser = malloc(sizeof(csv_parser));
	if (file_path == NULL) {
		parser->file_path = NULL;
	} else {
		size_t file_path_len = strlen(file_path);
		parser->file_path = malloc(file_path_len + 1);
		strcpy(parser->file_path, file_path);
	}
	parser->has_header = has_header;
	parser->error_message = NULL;
	if (delimiter == NULL) {
		parser->delimiter = ',';
	} else if (delimiter_is_accepted(delimiter)) {
		parser->delimiter = *delimiter;
	} else {
		parser->delimiter = '\0';
	}
	parser->header = NULL;
	parser->file_handler = NULL;
	parser->from_string = false;
	parser->csv_string = NULL;
	parser->csv_string_iter = 0;

	return parser;
}

csv_parser *init_parser_from_string(const char *csv_string, const char *delimiter)
{
	csv_parser *parser = init_parser(NULL, delimiter, false);
	parser->from_string = true;
	if (csv_string != NULL) {
		size_t csv_string_len = strlen(csv_string);
		parser->csv_string = malloc(csv_string_len + 1);
		strcpy(parser->csv_string, csv_string);
	}
	return parser;
}

void free_parser(csv_parser *parser)
{
	if (parser == NULL) {
		return;
	}
	if (parser->file_path != NULL) {
		free(parser->file_path);
	}
	if (parser->error_message != NULL) {
		free(parser->error_message);
	}
	if (parser->file_handler != NULL) {
		fclose(parser->file_handler);
	}
	if (parser->header != NULL) {
		free_row(parser->header);
	}
	if (parser->csv_string != NULL) {
		free(parser->csv_string);
	}
	free(parser);
}

void free_row(csv_row *row)
{
	for (size_t i = 0; i < row->num_fields; i++) {
		free(row->fields[i]);
	}
	free(row);
}

csv_row *get_header(csv_parser *parser)
{
	if (!parser->has_header) {
		set_error(parser,"Current csv_parser object does not have a header");
		return NULL;
	}
	if (parser->header == NULL) {
		parser->header = _get_row(parser);
	}
	return parser->header;
}

csv_row *get_row(csv_parser *parser)
{
	if (parser->has_header && parser->header == NULL) {
		parser->header = _get_row(parser);
	}
	return _get_row(parser);
}

size_t num_fields(csv_row *row)
{
	return row->num_fields;
}

char **get_fields(csv_row *row)
{
	return row->fields;
}

csv_row *_get_row(csv_parser *parser)
{
	size_t num_rows_realloc = 0;
	size_t accepted_fields = 64;
	size_t accepted_chars_in_field = 64;
	if (parser->file_path == NULL && (!parser->from_string)) {
		set_error(parser, "Supplied CSV file path is NULL");
		return NULL;
	}
	if (parser->csv_string == NULL && parser->from_string) {
		set_error(parser, "Supplied CSV string is NULL");
		return NULL;
	}
	if (parser->delimiter == '\0') {
		set_error(parser, "Supplied delimiter is not supported");
		return NULL;
	}
	if (!parser->from_string) {
		if (parser->file_handler == NULL) {
			parser->file_handler =
			    fopen(parser->file_path, "r");
			if (parser->file_handler == NULL) {
				int error_no = errno;
				const char *err_str = strerror(error_no);
				char *error_message =
				    malloc(1024 + strlen(err_str));
				strcpy(error_message, "");
				sprintf(error_message,
					"Error opening CSV file for reading: %s : %s",
					parser->file_path, err_str);
				set_error(parser, error_message);
				free(error_message);
				return NULL;
			}
		}
	}
	csv_row *row = malloc(sizeof(csv_row));
	row->fields = malloc(accepted_fields * sizeof(char *));
	row->num_fields = 0;
	size_t field_iter = 0;
	char *curr_field = malloc(accepted_chars_in_field);
	bool inside_complex_field = false;
	size_t curr_field_char_iter = 0;
	size_t series_of_quotes_length = 0;
	bool last_char_is_quote = false;
	bool is_eof = false;
	while (true) {
		char curr_char = (parser->from_string)
		    ? parser->csv_string[parser->csv_string_iter]
		    : fgetc(parser->file_handler);
		parser->csv_string_iter++;
		bool eof_indicator;
		if (parser->from_string) {
			eof_indicator = (curr_char == '\0');
		} else {
			eof_indicator = feof(parser->file_handler);
		}
		if (eof_indicator) {
			if (curr_field_char_iter == 0 && field_iter == 0) {
				set_error(parser, "Reached EOF");
				return NULL;
			}
			curr_char = '\n';
			is_eof = true;
		}
		if (curr_char == '\r') {
			continue;
		}
		if (curr_field_char_iter == 0 && !last_char_is_quote) {
			if (curr_char == '\"') {
				inside_complex_field = true;
				last_char_is_quote = true;
				continue;
			}
		} else if (curr_char == '\"') {
			series_of_quotes_length++;
			inside_complex_field =
			    (series_of_quotes_length % 2 == 0);
			if (inside_complex_field) {
				curr_field_char_iter--;
			}
		} else {
			series_of_quotes_length = 0;
		}
		if (is_eof
		    ||
		    ((curr_char == parser->delimiter
		      || curr_char == '\n') && !inside_complex_field)) {
			curr_field[last_char_is_quote
				? curr_field_char_iter - 1
				: curr_field_char_iter] = '\0';
			row->fields[field_iter] =
			    malloc(curr_field_char_iter + 1);
			strcpy(row->fields[field_iter], curr_field);
			free(curr_field);
			row->num_fields++;
			if (curr_char == '\n') {
				return row;
			}
			if (row->num_fields != 0
			    && row->num_fields % accepted_fields == 0) {
				row->fields = realloc(row->fields,
					((num_rows_realloc + 2) * accepted_fields) *
						     sizeof(char *));
				num_rows_realloc++;
			}
			accepted_chars_in_field = 64;
			curr_field = malloc(accepted_chars_in_field);
			curr_field_char_iter = 0;
			field_iter++;
			inside_complex_field = 0;
		} else {
			curr_field[curr_field_char_iter] = curr_char;
			curr_field_char_iter++;
			if (curr_field_char_iter == accepted_chars_in_field - 1) {
				accepted_chars_in_field *= 2;
				curr_field =
				    realloc(curr_field,
					    accepted_chars_in_field);
			}
		}
		last_char_is_quote = (curr_char == '\"') ? 1 : 0;
	}
}

bool delimiter_is_accepted(const char *delimiter)
{
	char actual_delimiter = *delimiter;
	if (actual_delimiter == '\n' || actual_delimiter == '\r'
	    || actual_delimiter == '\0' || actual_delimiter == '\"') {
		return false;
	}
	return true;
}

void set_error(csv_parser *parser, const char *error_message)
{
	if (parser->error_message != NULL) {
		free(parser->error_message);
	}
	size_t error_message_len = strlen(error_message);
	parser->error_message = malloc(error_message_len + 1);
	strcpy(parser->error_message, error_message);
}

const char *get_error(csv_parser *parser)
{
	return parser->error_message;
}
